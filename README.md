## Headless WordPress Frontend Vue

![Screenshot](./src/assets/screenshot.png)

#### With Load More Button
![Screenshot](./src/assets/screenshot_pagination.png)

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
