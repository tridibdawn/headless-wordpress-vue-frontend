import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        apiBaseUrl: '',
        allPosts: [],
        postDetails: {},
        showPostIndexLimit: 5,
        increasePostIndexBy: 3,
        allTags: [],
    },
    mutations: {
        updateApiBaseUrl: (state, payload) => {
            state.apiBaseUrl = payload;
        },
        updateAllPosts: (state, payload) => {
            state.allPosts = payload;
        },
        updatePostDetails: (state, payload) => {
            state.postDetails = payload;
        },
        updateShowPostIndexLimit: (state, payload) => {
            state.showPostIndexLimit = payload;
        },
        updateIncreasePostIndexBy: (state, payload) => {
            state.increasePostIndexBy = payload;
        },
        updateAllTags: (state, payload) => {
            state.allTags = payload;
        }
    }
});