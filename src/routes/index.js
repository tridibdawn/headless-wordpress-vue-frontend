import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '',
            name: 'Home',
            component: () => import('../views/Home.vue'),
        },
        {
            path: '/posts/:id',
            name: 'PostDetails',
            component: () => import('../views/PostDetails.vue'),
        }
    ]
});

export default router;