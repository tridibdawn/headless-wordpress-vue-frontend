import axios from 'axios';
const apiBaseUrl = `${process.env.VUE_APP_API_URL}`;
export default {
    getPosts() {
        return axios.get(`${apiBaseUrl}/wp-json/wp/v2/posts?_embed&per_page=100`);
    },
    getPostDetails(id) {
        return axios.get(`${apiBaseUrl}/wp-json/wp/v2/posts/${id}`);
    },
    getTags() {
        return axios.get(`${apiBaseUrl}/wp-json/wp/v2/tags?per_page=100`);
    }
}