import { mapState } from 'vuex';
import PostService from '@/services/PostService';
import moment from 'moment/moment';
export default {
    computed: {
        ...mapState([
            "apiBaseUrl",
            "allPosts",
            "postDetails",
            "showPostIndexLimit",
            "increasePostIndexBy",
            "allTags",
        ]),
        api_base_url: {
            get() {
                return this.apiBaseUrl;
            },
            set(val) {
                this.$store.commit('updateApiBaseUrl', val);
            }
        },
        all_posts: {
            get() {
                return this.allPosts;
            },
            set(val) {
                this.$store.commit('updateAllPosts', val);
            }
        },
        post_details: {
            get() {
                return this.postDetails;
            },
            set(val) {
                this.$store.commit('updatePostDetails', val);
            }
        },
        totalPosts() {
            return this.all_posts.length;
        },
        show_post_index_limit: {
            get() {
                return this.showPostIndexLimit;
            },
            set(val) {
                this.$store.commit('updateShowPostIndexLimit', val);
            }
        },
        increase_post_index_by: {
            get() {
                return this.increasePostIndexBy;
            },
            set(val) {
                this.$store.commit('updateIncreasePostIndexBy', val);
            }
        },
        showLoadMoreButton() {
            if(this.show_post_index_limit < this.totalPosts) {
                return true;
            } else {
                return false;
            }
        },
        all_tags: {
            get() {
                return this.allTags;
            },
            set(val) {
                this.$store.commit('updateAllTags', val);
            }
        }
    },
    methods: {
        getAllTags() {
            PostService.getTags()
            .then((res) => {
                this.all_tags = res.data;
            })
            .catch(error => console.error(error));
        },
        getAllPosts() {
            PostService.getPosts()
            .then((res) => {
                this.all_posts = res.data;
            })
            .catch(error => console.error(error));
        },
        removeSpecialCharacters(str) {
            return str && str.includes('[&hellip;]') ?  str.replace('[&hellip;]', '') : str;
        },
        getPostDetails(id) {
            PostService.getPostDetails(id)
            .then(res => {
                this.post_details = res.data;
            })
            .catch(error => console.error(error));
        },
        loadMorePosts() {
            this.show_post_index_limit = this.show_post_index_limit + this.increase_post_index_by;
        },
        getTagName(id) {
            let result = this.all_tags.find(el => el.id == id)?.name;
            return result ? result : 'N/A';
        }
    },
    filters: {
        dateFilter(date) {
            if(date && new Date(date)) {
                return moment(new Date(date)).format('MMMM DD, YYYY');
            } else {
                return date;
            }
        }
    }
}